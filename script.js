function showAlert(text){
	iziToast.warning({
		position: 'topCenter',
		title: "Alert ze Systému",
		message: text,
		displayMode: 'replace',
    	id: 'alert'
	})
}

function showSuccess(text){
	iziToast.success({
		position: 'topCenter',
		title: "Úspěch",
		message: text,
		displayMode: 'replace',
    	id: 'alert'
	})
}

function getNonce(){
	return Math.floor(Math.random() * 100000);
}

function apiCall(endpoint, parameters){
	let paramString = "?nonce=" + getNonce();
	if(parameters !== undefined){
		for (let key in parameters){
			paramString = paramString + "&" + key + "=" + encodeURIComponent(parameters[key]);
		}
	}

	let url = 'https://velky-bratr.cz/api/';
	return fetch(url + endpoint + paramString, {
		credentials: 'include'
	}).then(function(response) {
		return response.json();
	});
}

function isLoggedIn(){

	return apiCall('login/isLoggedIn').then(function(json) {
		if(json.status == 200){
			return json.response;
		} else if(json.error){
			showAlert("Systém vrátil následující chybu: " + json.error);
			return false;
		} else {
			showAlert("Něco se nepovedlo. Tady jsou dodatečné informace ze Systému: " + JSON.stringify(json));
		}
	});
}

function getCitizens(){

	return apiCall('map/getNearCitizens').then(function(json) {
		if(json.status == 200){
			return json.response;
		} else {
			if(json.error){
				showAlert("Systém vrátil následující chybu: " + json.error);
			} else {
				showAlert("Něco se nepovedlo. Tady jsou dodatečné informace ze Systému" + JSON.stringify(json));
			}
			return Promise.reject();
		}
	});
}

function getSabotages(longitude, latitude){

	return apiCall('map/getNearSabotages').then(function(json) {
		if(json.status == 200){
			return json.response;
		} else {
			if(json.error){
				showAlert("Systém vrátil následující chybu: " + json.error);
			} else {
				showAlert("Něco se nepovedlo. Tady jsou dodatečné informace ze Systému" + JSON.stringify(json));
			}
			return Promise.reject();
		}
	});
}

function getInterrogations(){

	return apiCall('map/getNearInterrogations').then(function(json) {
		if(json.status == 200){
			return json.response;
		} else {
			if(json.error){
				showAlert("Systém vrátil následující chybu: " + json.error);
			} else {
				showAlert("Něco se nepovedlo. Tady jsou dodatečné informace ze Systému" + JSON.stringify(json));
			}
			return Promise.reject();
		}
	});
}

function saveCoords(longitude, latitude){
	/*alert("longitude" + longitude + " latitude" + latitude);
	return Promise.reject();*/
	return apiCall('location/saveCoordinates', {longitude: longitude, latitude: latitude, locationManuallySelected: false}).then((json) => {
		if(json.status == 200) {
			return true;
		} else {
			if(json.error){
				showAlert("Systém vrátil následující chybu: " + json.error);
			} else {
				showAlert("Něco se nepovedlo. Tady jsou dodatečné informace ze Systému" + JSON.stringify(json));
			}
			return Promise.reject();
		}
	})
	//https://velky-bratr.cz/api/location/saveCoordinates?latitude=49.2118016&longitude=16.602726399999998&locationManuallySelected=false
}

let generateFakeData = () => {
	let result = [];
	for(let i = 0; i < 100; i++){
		result.push({
			longitude: 15.005808081643 + (Math.random() - 0.5) * 0.01,
			latitude: 49.142149746932 + (Math.random() - 0.5) * 0.01,
			marker: "myself"

		});
	}

	return result;
}

let generateGeoJson = (input, layer) => {
	layer = "stopy";
	console.log('Generating geojson...');
	let finalPlaces = [];
	for(let place of input) {
		finalPlaces.push({
			type: "Feature",
			geometry: {
				type: "Point",
				coordinates: [parseFloat(place.longitude), parseFloat(place.latitude)]
			},
			properties: {
				icon: place.marker,
				fullTitle: "Title",
				description: "<strong>This is it!</strong> <a href='tomaszelina.cz'>Lorem ipsum</a>, also dolor sit and amet. Tetrashit."
			}
		});
	}

	console.log(finalPlaces);

	return [
		{
			"id": layer,
			"type": "symbol",
			"source": layer,
			"layout": {
				"icon-allow-overlap": true,
				"icon-image": "{icon}",
				"text-field": "{title}",
				"text-font": ["Open Sans Regular"],
				"text-offset": [0, 0.6],
				"text-anchor": "top",
				//"text-halo-color": "rgba(0, 52, 102, 0.8)"
			}
		},
		{
			"type": "geojson",
			"data": {
				"type": "FeatureCollection",
				"features": finalPlaces
			},
			//"cluster": true
		}

	];

}

let setStatus = (status) => {
	$(".statusText").html(status);
}

let initMap = (geoJson) => {

	$("#map").html("");
	$("#map").css("text-align", "initial");


	let map = new mapboxgl.Map({
		container: 'map', // container id
		style: 'https://blahonie.tomaszelina.cz/styles/style-offline.json', // stylesheet location
		center: [15.005808081643, 49.142149746932],//[15.39739,49.97903825164818], // starting position [lng, lat]
		zoom: 15 // starting zoom
	});

	return new Promise(resolve => map.on('load', () => resolve(map)));
}

let centerOnPlayer = function(map, citizens){
	for(let place of citizens){
		if(place.marker === "myself"){
			map.setZoom(14);
			map.panTo([place.longitude, place.latitude]);
		}
	}
}

let presentLayers = [];

let addLayer = (map, layer) => {
	if(presentLayers.includes(layer[0].id)){
		map.getSource(layer[0].id).setData(layer[1].data);
	} else {
		map.addSource(layer[0].source, layer[1]);
		presentLayers.push(layer[0].id);
		map.addLayer(layer[0]);
		map.on('click', layer[0].id, function(e) {
			let coordinates = e.features[0].geometry.coordinates.slice();
			let description = e.features[0].properties.description;
			let title = e.features[0].properties.fullTitle;
			// Ensure that if the map is zoomed out such that multiple
			// copies of the feature are visible, the popup appears
			// over the copy being pointed to.
			while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
				coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
			}

			new mapboxgl.Popup()
				.setLngLat(coordinates)
				.setHTML("<h3>" + title + "</h3><div class='popupDescription'" + description + "</div>")
				.addTo(map);
		});
	}

}

let addData = function(map){
	let afterResolving = function(name){
		return (result) => {
			addLayer(map, generateGeoJson(result, name));
			console.log(name);
			return result;
		}
	};

	afterResolving("whatever")(generateFakeData());

	/*getCitizens().then(afterResolving('people')).then(function(result){
		centerOnPlayer(map, result);
	});
	getSabotages().then(afterResolving('sabotages'));
	getInterrogations().then(afterResolving('interrogations'));*/

}




let loggedInPromise = isLoggedIn()
let mapPromise = initMap();

loggedInPromise.then(result => {
	if(!result) {
		showAlert("Pouze přihlášení vidí na mapě stopy. To ty nejsi :( Přihlaš se na velky-bratr.cz a vrať se za moment.");
		return Promise.reject();
	}

	//watchMyGPS(false);
})



let map = undefined;
let lastUpdate = 0;
let lastCoords = [];

Promise.all([loggedInPromise, mapPromise]).then(function(results){
	map = results[1];
	console.log("All resolved");
	if(results[0]){
		addData(map);
	}

}).then(() => {
	$("#menutoggle").removeClass("hidden");
});

function reloadMap(){
	addData(map);
}

function sendCoordinates(latitude, longitude) {
	let now = new Date().getTime();
	if(now - lastUpdate < 60000){
		console.log("Not updating, last updated " + (now - lastUpdate) + " ms ago");
		return;
	}
	lastUpdate = now;
    saveCoords(latitude, longitude).then((result) => {
    	if(result){
    		showSuccess('GPS pozice byla odeslána na server! :)');
    		reloadMap();
    	}
    })

}

/*function watchMyGPS(forceUpdate) {
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        sendCoordinates( longitude, latitude);
    }
    function error() {
        showAlert("Odesílání pozice Systému selhalo. Je povolena GPS na tvém zařízení a je tato poloha dostatečně přesná?");
    }
    var opts = {
        enableHighAccuracy: true,
        maximumAge: 20000
    };

    if(forceUpdate){
    	navigator.geolocation.getCurrentPosition(success, error, opts);
    } else {
    	navigator.geolocation.watchPosition(success, error, opts);
    }

}*/



$().ready(() => {
	$("#menutoggle").click(()=> {
		$("#mainmenu").toggleClass('hidden');
	});

	$(".forceRefresh").click(()=> {
		lastUpdate = 0;
		watchMyGPS(true);
	});
	$(".forceDataRefresh").click(()=> {
		addData();
	});

});














