/*
   Prosím, nekamnujte mě za hnusný javascript...
   Jsem jenom ubohý backendista, co běžně píše v Javě...
 */

function showAlert(text){
    iziToast.warning({
        position: 'topCenter',
        title: "Alert ze Systému",
        message: text,
        displayMode: 'replace',
        id: 'alert'
    })
}

function showSuccess(text){
    iziToast.success({
        position: 'topCenter',
        title: "Úspěch",
        message: text,
        displayMode: 'replace',
        id: 'alert'
    })
}

const markers = ["myself", "party", "resistance", "none", "interrogation_party", "interrogation_resistance",
    "cctv_party", "cctv_none", "cctv_resistance", "sabotage_party", "sabotage_resistance", "vaporization_party", "vaporization_resistance", "gathering"];
const markerTitles = ["Tvoje poloha", "Straník", "Odbojář", "Občan", "Výslech za stranu", "Výslech za odboj",
    "CCTV - Strana", "CCTV", "CCTV - odboj", "Sabotáž za Stranu", "Sabotáž za Odboj", "Vaporizace Straníka", "Vaporizace odbojáře", "Setkání"];
const markerDescriptions = ["Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami", "Před 1234 hodinami"];
const markerUrls = ["", "https://tomaszelina.cz/test.html?nonce=asdg", "https://tomaszelina.cz/test.html?nonce=asdg", "https://tomaszelina.cz/test.html?nonce=asdg", "", "", "", "", "", "", "", "", "", ""];

let generateFakeData = () => {
    let result = [];

    for(let i = 0; i < 20; i++){
        let index = Math.floor(Math.random() * markers.length);

        result.push({
            longitude: 15.0025 + (Math.random() - 0.5) * 0.005,
            latitude: 49.1443 + (Math.random() - 0.5) * 0.005,
            marker: markers[index],
            found: Math.random() > 0.5,
            time: Math.ceil(Math.random() * 59) + " " + ['s', 'm', 'h'][Math.floor(Math.random() * 3)],
            markerUrl: markerUrls[index],
            title: markerTitles[index],
            description: markerDescriptions[index]
        });
    }

    return result;
};

let markerClickHandler = (map, e) => {
    let coordinates = e.features[0].geometry.coordinates.slice();
    let description = e.features[0].properties.description;
    let title = e.features[0].properties.fullTitle;
    let fetchUrl = e.features[0].properties.markerUrl;
    let id = e.features[0].properties.id;
    let urlDefined = fetchUrl !== undefined && fetchUrl.length > 0;
    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    // Dirty hack, remove when possible
    let sourceName = e.features[0].source;

    let data = presentSources[sourceName];
    if(data !== undefined){
        let features = data.data.features;
        for(let i = 0; i < features.length; i++){
            if(features[i].properties.id === id){
                features[i].properties.found = true;
            }
        }
    }

    addMapboxSource(map, sourceName, data);

    if(urlDefined){
        description = "Načítám...";
    }

    let makePopup = (title, description) => {
        let html =  makePopupTitle(title) + makePopupDescription(description);
        new mapboxgl.Popup()
            .setLngLat(coordinates)
            .setHTML(html)
            .addTo(map);
    };



    if(urlDefined){
        fetch(fetchUrl, /*{credentials: 'include'}*/)
            .then((response) => {
                if(response.status === 200){
                    response.text().then(text => {
                        makePopup(title, text);
                    });
                } else {
                    showAlert("Nemůžu načíst kartu. Systém vrátil kód " + response.status + ". Více info v konzoli");
                    console.error(response);
                }

            })
            .catch((error) => {
                showAlert("Nemůžu načíst kartu. Více info v konzoli.");
                console.error(error);
            });
    } else {
        makePopup(title, description);
    }

    e.originalEvent.markerProcessed = true;
};

let id = 1000;

let generateGeoJson = (input, layer) => {
    console.log('Generating geojson...');
    let finalPlaces = [];
    for(let place of input) {
        finalPlaces.push({
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: [parseFloat(place.longitude), parseFloat(place.latitude)]
            },
            properties: {
                id: (id++),
                icon: place.marker,
                time: place.time,
                found: place.found === undefined ? false : place.found,
                fullTitle: place.title,
                description: place.description,
                markerUrl: place.markerUrl
            }
        });
    }


    let layers = [
        {
            "id": layer,
            "type": "symbol",
            "source": layer,
            "layout": {
                "icon-allow-overlap": true,
                "icon-image": "{icon}",
                "text-field": "{time}",
                "text-font": ["Open Sans Bold"],
                "text-offset": [0, 0.9],
                "text-anchor": "top",

            },
            "paint": {
                "text-color": "#f3c611",
                "text-halo-width": 1.5,
                "text-halo-color": "rgba(0, 52, 102, 0.8)"
            },
            filter: [
                "==",
                "found",
                false
            ],
            clickHandler: markerClickHandler
        },
        {
            "id": layer + "-found",
            "type": "symbol",
            "source": layer,
            "layout": {
                "icon-allow-overlap": true,
                "icon-image": "{icon}",
                "text-field": "{time}",
                "text-font": ["Open Sans Bold"],
                "text-offset": [0, 0.9],
                "text-anchor": "top",

            },
            "paint": {
                "icon-opacity": 0.5,
                "text-color": "#cccccc",
                "text-halo-width": 1.5,
                "text-halo-color": "rgba(0, 52, 102, 0.8)"
            },
            filter: [
                "==",
                "found",
                true
            ],
            clickHandler: markerClickHandler
        },
    ];

    let result = {
        layers: layers,
        sources: {}
    };

    result.sources[layer] = {
        "type": "geojson",
        "data": {
            "type": "FeatureCollection",
            "features": finalPlaces
        },
        "cluster": false
    };

    return result;

};

let createGeoJSONCircle = function (center, radiusInKm, points) {
    if (!points) points = 64;

    var coords = {
        latitude: center[1],
        longitude: center[0]
    };

    var km = radiusInKm;

    var ret = [];
    var distanceX = km / (111.320 * Math.cos(coords.latitude * Math.PI / 180));
    var distanceY = km / 110.574;


    for (var i = 0; i < points; i++) {
        theta = (i / points) * (2 * Math.PI);
        x = distanceX * Math.sin(theta);
        y = distanceY * Math.cos(theta);


        ret.push([coords.longitude + x, coords.latitude + y]);
    }
    ret.push(ret[0]);

    return {
        "type": "Feature",
        "geometry": {
            "type": "Polygon",
            "coordinates": [ret]
        }
    }
};


let generateCircles = (input, layerName, radius) => {
    console.log('Generating circles...');
    let finalPlaces = [];
    for(let place of input) {
        if(place.marker === 'myself' || place.marker === 'cctv_myself'){

            finalPlaces.push(createGeoJSONCircle([parseFloat(place.longitude), parseFloat(place.latitude)], radius, 64));
        }
    }

    let layers = [{
        "id": layerName,
        "type": "fill",
        "source": layerName,
        "paint": {
            "fill-color": "#f1c40f",
            "fill-opacity": 0.6
        }
    }];

    let sources = {};
    sources[layerName] = {
        "type": "geojson",
        "data": {
            "type": "FeatureCollection",
            "features": finalPlaces
        },
    };

    return {
        layers: layers,
        sources: sources
    };
};


let initMap = (center, zoom) => {
    let mapElement = document.getElementById("map");
    mapElement.innerHTML = "";
    mapElement.style.textAlign = "initial";



    let map = new mapboxgl.Map({
        pitchWithRotate: false,
        dragRotate: false,
        container: 'map', // container id
        style: 'https://blahonie.tomaszelina.cz/styles/style-offline.json', // stylesheet location
        center: center,
        zoom: zoom // starting zoom
    });

    return new Promise(resolve => map.on('load', () => resolve(map)));
};

let updateMap = async (mapPromise, geoJson) => {
    addLayer(await mapPromise, geoJson);
};

let centerOnPlayer = (map, layer) => {
    for(let source of Object.values(layer.sources)){
        for(let place of source.data.features){
            if(place.marker === "myself"){
                map.setZoom(14);
                map.panTo([place.longitude, place.latitude]);
                return;
            }
        }
    }
    console.log(layer);

};

let presentLayers = [];

let makePopupTitle = (title) => {
    if(title !== undefined && title.length > 1){
        return "<h3>" + title + "</h3>";
    }

    return "";
};

let makePopupDescription = (desc) => {
    if(desc !== undefined && desc.length > 1){
        return "<div class='popupDescription'>" + desc + "</div>";
    }

    return "";
};

let presentSources = {};

let addMapboxSource = (map, name, source) => {
    if(presentSources[name]!== undefined){
        map.getSource(name).setData(source.data);
        return;
    }

    map.addSource(name, source);
    presentSources[name] = source;
};

let addMapboxLayer = (map, layer) => {
    if(presentLayers.includes(layer.id)){
        return;
    }
    let lastLayer = presentLayers[presentLayers.length - 1];
    presentLayers.push(layer.id);
    map.addLayer(layer, lastLayer);

    if(layer.clickHandler !== undefined){
        map.on('click', layer.id, (e) => layer.clickHandler(map, e));
    }


};

let addLayer = (map, layer) => {
    for(let name of Object.keys(layer.sources)){
        addMapboxSource(map, name, layer.sources[name]);
    }

    for(let mapboxLayer of layer.layers){
        addMapboxLayer(map, mapboxLayer);
    }

};


