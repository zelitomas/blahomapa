#! /bin/bash
mkdir temp
if [ "$1" != "" ]; then scale=$1; else scale=1; fi
if [ "$scale" == "1" ]; then spritefile=sprite ; else spritefile=sprite@$scale"x" ; fi
files=""
width=$((50*$scale))
i=0
echo \{ > $spritefile.json
for f in ./source/*.svg
do
    filename=`basename $f`
    echo processing $f
    name=$(echo "$filename" | cut -f 1 -d '.')
    inkscape --export-png="export_$name.png" --export-width=$width --export-height=$width $f
    files=$files"./temp/export_$name.png "
    if [ "$i" != "0" ]; then echo "," >> $spritefile.json; fi
    echo "
    \"$name\": {
        \"height\": $width,
        \"pixelRatio\": $scale,
        \"width\": $width,
        \"x\": $(($i*$width)),
        \"y\": 0
    }" >> $spritefile.json
    i=$(($i+1))
done

echo \} >> $spritefile.json

mv export_*.png ./temp
echo $files
gm convert $files +append $spritefile.png
rm -r temp

